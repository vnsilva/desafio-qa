#language:  pt

Funcionalidade: Ligações

  Como usuário gostaria de realizar ligações de video e de voz
  assim também como encerra-las e coloca-las em viva voz

  Cenário: Realizar ligação de Voz
    Dado que estou com o whatsApp aberto
    E toco na Lupa
    E digito "Jéssica"
    Quando eu toco a conversa
    E toco no icone do telefone
    Então a ligação de voz é realizada
    E eu consigo falar com o meu contato

  Cenário: Relizar ligação de video
    Dado que estou com o whatsApp aberto
    E toco na lupa
    E digito "Jéssica"
    Quando eu toco na conversa
    E toco na icone da camera
    Então a ligação de video é realizada
    E eu consigo visualizar meu contato
    E eu consigo visualizar a mim
    E conseguimos conversar

  Cenário: Verificar se é possível encerrar uma ligação de voz
    Dado que estou com o whatsApp aberto
    E toco na Lupa
    E digito "Jéssica"
    E eu toco a conversa
    Quando toco no icone do telefone
    E a ligação de voz é realizada
    E eu toco no icone de encerrar ligação
    Então a ligação é encerrada

   Cenário: Verificar se é possĩvel encerrar uma ligação de voz
     Dado que estou com o whatsApp aberto
     E toco na Lupa
     E digito "Jéssica"
     E eu toco a conversa
     Quando toco no icone da camera
     E a ligação de video é realizada
     E eu toco no icone de encerrar ligação
     Então a ligação é encerrada

   Cenário: Verificar se é colocar uma ligação de voz em viva voz
     Dado que estou com o whatsApp aberto
     E toco na Lupa
     E digito "Jéssica"
     E eu toco a conversa
     Quando toco no icone de telefone
     E a ligação de voz é realizada
     Então eu toco no icone de viva voz
     E o som é transferido para o auto falante

   Cenário: Verificar se é possível navegar pela applicação durante uma ligação de video
     Dado que estou com o whatsApp aberto
     E toco na Lupa
     E digito "Jéssica"
     E eu toco a conversa
     Quando toco no icone da camera
     E a ligação de video é realizada
     E eu toco no icone mensagem
     Então eu consigo navegar pela apliação normalmente

   Cenário: Verificar se é possível realizar uma ligação de video em um smartphone sem camera
     Dado que estou com o whatsApp aberto
     E toco na Lupa
     E digito "Jéssica"
     E eu toco a conversa
     Quando toco no icone da camera
     Então eu visualizo a mensagem "Smartphone não compatĩvel com essa tecnologia"

   Cenário: Verificar se é possivel desligar o microfone do smartphone durante uma ligação de voz
     Dado que estou com o whatsApp aberto
     E toco na Lupa
     E digito "Jéssica"
     E eu toco a conversa
     Quando toco no icone do telefone
     E a ligação de voz é realizada
     E eu toco no icone de mute
     Então o meu microfone é desligado
     E o meu contato não me escuta

   Cenário: Verificar se é possivel desligar o microfone do smartphone durante uma ligação de video
    Dado que estou com o whatsApp aberto
    E toco na Lupa
    E digito "Jéssica"
    E eu toco a conversa
    Quando toco no icone da camera
    E a ligação de video é realizada
    E eu toco no icone de mute
    Então o meu microfone é desligado
    E o meu contato não me escuta

  Cenário: Verificar se é possivel inverter a camera durante a ligação de video
    Dado que estou com o whatsApp aberto
    E toco na Lupa
    E digito "Jéssica"
    E eu toco a conversa
    Quando toco no icone da Camera
    E a ligação de video é realizada
    E eu toco no icone de inversão de camera
    Então a camera é alterada para a traseira
    E eu toco novamente
    E a camera é alterada para dianteira
