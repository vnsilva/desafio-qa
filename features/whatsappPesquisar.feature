#language:  pt

Funcionalidade: Pesquisar

  Como usuário do whatsApp android
  Eu gostaria de pesquisar por um contato, conversa, grupo, status e Ligaçõs


  Cenário: Pesquisar por um Contato Previamente Cadastrado
    Dado que eu estou com o whatsApp aberto
    E eu tenho um usuário cadastrado com conta ativa no whatsapp
    Quando eu toco na lupa
    E Digito "João"
    Então o contato é exibido na lista de resultados

  Cenário: Pesquisar por um Contato que não está Cadastrado
    Dado que eu estou na aba de conversas
    E possuo conversas no smartphone
    E digito "Valeska"
    Então eu visualizo a mensagem "Sem resultados para Valeska"

  Cenário: Pesquisar por um trecho de conversa que esteja lista
    Dado que eu estou na aba de conversas
    E possuo conversas no smartphone
    Quando eu toco na lupa
    E digito "comprar pão"
    Então eu visualizo o trecho na lista de resultados

  Cenário: Pesquisar por status que esteja na lista
    Dado estou na aba de status
    E possuo status de contatos no whatsApp
    Quando eu toco na lupa
    E Digito "Claudio"
    Então Visualizo o status do Claudio na lista de resultados

  Cenário: Pesquisar por um status não esteja na lista
    Dado estou na aba de status
    E possuo status de contatos no whatsApp
    Quando eu toco na lupa
    E digito "Henrique"
    Então eu visualizo a mensagem "Sem resultados para Henrique"

  Cenário: Pesquisar por ligações que estejam na lista
    Dado estou na aba de ligações
    E possuo ligações no wahtsApp
    Quando eu toco na lupa
    E digito "Fernando"
    Então visualizo a ligação do fernando na lista de resultados

  Cenário: Pesquisar por ligações que não estejam na lista
    Dado estou na aba de ligações
    Quando eu toco na lupa
    E digito "Claudio"
    Então eu visualizo a mensagem "Sem resultados para Claudio"
