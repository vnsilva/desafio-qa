require_relative '../lib/checkout'

def price(goods)
  co = CheckOut.new(RULES)
  goods.split(//).each { |item| co.scan(item) }
  co.total
end


describe ".mix" do
  context "Quando um item de cada forem adicionados ao carrinho" do
    it "Deve retornar o preço total dos items somadosde ~ 115" do
      expect(price('ABCD') ).to eql(115)
    end
  end
end

describe ".unidade" do
  context "Quando três intens de cada forem adicionados ao carrinho" do
    it "Deve retornar o preço total dos itens somados ~ 345" do
      expect(price('AAABBBCCCDDD') ).to eql(310)
    end
  end
end

