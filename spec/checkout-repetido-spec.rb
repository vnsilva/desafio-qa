require_relative '../lib/checkout'

def price(goods)
  co = CheckOut.new(RULES)
  goods.split(//).each { |item| co.scan(item) }
  co.total
end


describe ".repetido" do
  context "Quando dois intens A forem adicionados ao carrinho" do
    it "Deve retornar o preço de dois Itens A ~ 100" do
      expect(price('AA') ).to eql(100)
    end
  end
end

describe ".repetido" do
  context "Quando três intens A forem adicionados ao carrinho" do
    it "Deve retornar o preço promocional de três Itens A ~ 130" do
      expect(price('AAA') ).to eql(130)
    end
  end
end

describe ".repetido" do
  context "Quando quatro intens A forem adicionados ao carrinho" do
    it "Deve retornar o preço promocional de três Itens A mais um com preço normal ~ 180" do
      expect(price('AAAA') ).to eql(180)
    end
  end
end

describe ".repetido" do
  context "Quando seis intens A forem adicionados ao carrinho" do
    it "Deve retornar o preço promocional de três Itens A vezes dois ~ 260" do
      expect(price('AAAAAA') ).to eql(260)
    end
  end
end

describe ".repetido" do
  context"Quando dois itens B forem adicionados ao carrinho" do
    it "Deve retornar o preço promocional de 2 Itens B ~ 45" do
      expect(price('BB') ).to eql(45)
    end
  end
end

describe ".repetido" do
  context"Quando cinco itens B forem adicionados ao carrinho" do
    it "Deve retornar o preço promocional de 5 Itens B ~ 120" do
      expect(price('BBBBB') ).to eql(120)
    end
  end
end

describe ".repetido" do
  context"Quando 10 itens C forem adicionados ao carrinho" do
    it "Deve retornar o preço de 10 Itens C ~ 200" do
      expect(price('CCCCCCCCCC') ).to eql(200)
    end
  end
end

describe ".repetido" do
  context"Quando 10 itens D forem adicionados ao carrinho" do
    it "Deve retornar o preço de 10 Itens D ~ 150" do
      expect(price('DDDDDDDDDD') ).to eql(150)
    end
  end
end
