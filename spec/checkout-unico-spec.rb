require_relative '../lib/checkout'

def price(goods)
  co = CheckOut.new(RULES)
  goods.split(//).each { |item| co.scan(item) }
  co.total
end


describe ".unico" do
  context "Quando um item A for adicionado ao carrinho" do
    it "Deve retornar o preço de um Item A ~ 50" do
      expect(price('A')).to eql(50)
    end
  end
end

describe ".unico" do
  context"Quando um item B for adicionado ao carrinho" do
    it "Deve retornar o preço de um Item B ~ 30" do
      expect(price('B') ).to eql(30)
    end
  end
end

describe ".unico" do
  context"Quando um item C for adicionado ao carrinho" do
    it "Deve retornar o preço de um Item C ~ 20" do
      expect(price('C') ).to eql(20)
    end
  end
end

describe ".unico" do
  context"Quando um item D for adicionado ao carrinho" do
    it "Deve retornar o preço de um Item B ~ 15" do
      expect(price('D') ).to eql(15)
    end
  end
end


